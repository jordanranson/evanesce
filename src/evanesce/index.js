import anime from 'animejs'
import createTransition from 'gl-transition'
import createTexture from 'gl-texture2d'

function defaultOptions () {
  return {
    canvas: document.createElement('canvas'),
    initialImage: document.createElement('canvas'),
    transition: undefined,
    easing: 'linear',
    duration: 1000,
    params: {},
    highPerformance: true,
    disableStyles: true
  }
}

export default class Evanesce extends EventTarget {
  get width () {
    return this.canvas.width
  }

  get height () {
    return this.canvas.height
  }

  get ready () {
    return !!(this.transitionEffect && this.transitionAnimation && this.transitionFrom && this.transitionTo)
  }

  get pixelRatio () {
    let pixelRatio = window.devicePixelRatio
    if (this.highPerformance) pixelRatio = Math.min(pixelRatio, 2)
    return pixelRatio
  }

  constructor (options) {
    super()

    options = {
      ...defaultOptions(),
      ...options
    }

    this.canvas = options.canvas
    this.textures = {}
    this.lastTime = performance.now()
    this.renderHandler = this.render.bind(this)
    this.highPerformance = options.highPerformance
    this.disableStyles = options.disableStyles

    this.prepareRenderingContext()
    this.prepareDrawingSurface()
    this.resize()

    this.transitionProgress = 0
    this.transitionParams = options.params
    this.transitionPlaying = false
    this.transitionAnimation = undefined
    this.setTransition(options.transition)

    this.prepareAnimation(options.duration, options.easing)
    this.prepareForRendering()
  }

  prepareRenderingContext () {
    const gl = this.canvas.getContext('webgl') || this.canvas.getContext('experimental-webgl')
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true)

    this.gl = gl
  }

  prepareDrawingSurface () {
    const { gl } = this

    const buffer = gl.createBuffer()
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer)
    gl.bufferData(
      gl.ARRAY_BUFFER,
      new Float32Array([-1, -1, -1, 4, 4, -1]),
      gl.STATIC_DRAW
    )
  }

  prepareAnimation (duration, easing) {
    this.transitionAnimation = anime({
      targets: this,
      autoplay: false,
      transitionProgress: 100,
      duration,
      easing,
      complete: () => {
        this.dispatchEvent(new Event('complete'))
      }
    })
  }

  async prepareForRendering () {
    await this.setFrom(document.createElement('canvas'))
    await this.setTo(document.createElement('canvas'))

    this.render()
  }

  resize () {
    const { width, height } = this.canvas.getBoundingClientRect()
    const pixelRatio = this.pixelRatio

    this.canvas.width = width*pixelRatio
    this.canvas.height = height*pixelRatio

    if (!this.disableStyles) {
      this.canvas.style.width = `${width}px`
      this.canvas.style.height = `${height}px`
    }

    this.gl.viewport(0, 0, width*pixelRatio, height*pixelRatio)
  }

  loadImage (src) {
    return new Promise((resolve, reject) => {
      const image = new Image()

      image.onload = () => { resolve(image) }
      image.onerror = reject
      image.src = src
    })
  }

  async createTexture (image, cacheKey) {
    cacheKey = cacheKey || image

    if (typeof cacheKey === 'string' && this.textures[cacheKey]) {
      return this.textures[cacheKey]
    }

    if (typeof image === 'string') {
      try {
        image = await this.loadImage(image)
      } catch (err) {
        throw err
      }
    }

    const { gl } = this

    const texture = createTexture(gl, image)
    texture.minFilter = gl.LINEAR
    texture.magFilter = gl.LINEAR

    if (typeof cacheKey === 'string') this.textures[cacheKey] = texture

    return texture
  }

  async setFrom (image, cacheKey) {
    cacheKey = cacheKey || image
    const texture = await this.createTexture(image, cacheKey)
    this.transitionFrom = texture
  }

  async setTo (image, cacheKey) {
    cacheKey = cacheKey || image
    const texture = await this.createTexture(image, cacheKey)
    this.transitionTo = texture
  }

  setTransition (transition, duration, params) {
    if (typeof duration === 'number') this.transitionAnimation.duration = duration
    if (typeof params === 'object') this.transitionParams = params

    this.transitionEffect = createTransition(this.gl, transition)
  }

  setDuration (duration) {
    this.transitionAnimation.duration = duration
  }

  setProgress (pct) {
    this.transitionAnimation.seek(pct * this.transitionAnimation.duration)
  }

  setEasing (easing) {
    this.transitionAnimation.easing = easing
  }

  setParams (params) {
    this.transitionParams = params
  }

  async transition (to) {
    if (this.transitionProgress !== 0) {
      this.transitionFrom = this.transitionTo
    }

    await this.setTo(to)
    this.play(true)

    return new Promise((resolve) => {
      const handler = () => {
        resolve()
        this.removeEventListener('complete', handler)
      }
      this.addEventListener('complete', handler)
    })
  }

  play (fromStart) {
    if (fromStart) this.restart()
    this.transitionAnimation.play()
  }

  pause () {
    this.transitionAnimation.pause()
  }

  restart () {
    this.transitionAnimation.restart()
  }

  reverse () {
    this.transitionAnimation.reverse()
  }

  render () {
    if (this.destroying) return

    if (this.ready) {
      this.transitionEffect.draw(
        this.transitionProgress/100,
        this.transitionFrom,
        this.transitionTo,
        this.width,
        this.height,
        this.transitionParams
      )
    }

    requestAnimationFrame(this.renderHandler)
  }

  destroy () {
    this.destroying = true
  }
}
